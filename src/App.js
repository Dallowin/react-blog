import React, { useState } from 'react';
import { Routes, Route, useNavigate } from "react-router-dom";

import { BlogItem } from './components/BlogItem';
import { HomePage } from './components/HomePage';
import { ModalCreatePost } from "./components/ModalCreatePost"

import './App.css';

// Генерация уникального айди
const uid = () => {
    return Date.now().toString(36) + Math.random().toString(36).substr(2);
}

function App() {
    const navigate = useNavigate()

    // Переменные
    const [posts, setPosts] = useState([]);
    const [isShowing, setIsShowing] = useState(false);
    const [isEditing, setIsEditing] = useState(false);
    const [editingID, setEditingID] = useState("")


    // Состояние модального окна
    const toggleOpenModal = (status, id) => {
        setIsShowing(!isShowing)

        switch(status) {
            case 'add':
                setIsEditing(false)
                break
            case 'edit': 
                setIsEditing(!isEditing)
                setEditingID(id)
                break
            default:
                setIsShowing(false)
                setIsEditing(false)
        }
    }

    // Добавление нового элемента в массив useState
    const addNewPost = value => {
        setPosts(prevPosts => [
            ...prevPosts,
            {
                uid: uid(),
                createdAt: new Date().toString(),
                title: value.title,
                description: value.description
            }
        ])
    }

    // Изменение элемента в массиве useState
    const changePost = value => {
        // Копируем массив во временную переменную
        const oldValues = [...posts]
        // Ищем индексное значение элемента в массиве
        const changeIndex = oldValues.findIndex(post => value.uid === post.uid)
        // Делаем прямую замену через индекс
        oldValues[changeIndex] = value
        // Обновляем массив
        setPosts(oldValues)
    }

    // Удаление элемента в массиве useState
    const deletePost = id => {
        // Копируем массив во временную переменную
        const oldValues = [...posts]
        // Ищем индексное значение элемента в массиве
        const changeIndex = oldValues.findIndex(post => id === post.uid)
        // Делаем прямую замену через индекс
        oldValues.splice(changeIndex, 1)
        // Обновляем массив
        setPosts(oldValues)
        // Редирект на главную страницу
        navigate("/")
    }

    return (
        <div className="App">
            <Routes className="App-header">
                <Route path="/"     element={ <HomePage posts={ posts } toggleCreateModal={ toggleOpenModal } /> } />
                <Route path={`:id`}   element={ <BlogItem posts={ posts } toggleEditModal={ toggleOpenModal } deletePost={deletePost} /> } />
            </Routes>

            { isShowing && <ModalCreatePost posts={posts} addPost={ addNewPost } changePost={changePost} closeModal={ toggleOpenModal } isEdit={isEditing} editID={editingID} />  }
        </div>
    );
}

export default App;
