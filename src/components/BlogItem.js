import { Link, useParams } from "react-router-dom"
import "./BlogItem.css"

export function BlogItem({ posts, toggleEditModal, deletePost }) {
    // Получение параметра с роута
    let { id } = useParams();

    // Поиск в списке по параметру роута
    const getPostByUID = () => {
        return posts.find(post => post.uid === id)
    }

    return (
        <div className="App-header">
            <div className="page">
                
                <small className="page-date">{ getPostByUID().createdAt }</small>

                <h3 className="page-title">{ getPostByUID().title }</h3>
                <p className="page-description">{ getPostByUID().description }</p>

                <Link className="page-home" to="/">Go home</Link>

                <button className="button add-post__button" onClick={ () => toggleEditModal('edit', id) }>Edit Post</button>

                <button className="button remove-post__button" onClick={ () => deletePost(id) }>Remove Post</button>
            </div>
        </div>
    )
}
