import { Link } from "react-router-dom";

export function HomePage({ posts, toggleCreateModal, description }) {
    
    // Рендеринг списка постов
    const postsItems = posts.map((post) =>
        <div className='post-item' key={post.uid}>
            <h3 title={post.title}>{ post.title }</h3>
            <p>{ post.description }</p>
            <Link to={post.uid}>Перейти</Link>
        </div>
    );

    return (
        <header className="App-header">
            <h1>Blog</h1>

            <button className='button add-post__button' onClick={ () => toggleCreateModal('add') }>Add new Post</button>

            <div className='posts-list'>
                { postsItems }
            </div>
        </header>
    )
}
