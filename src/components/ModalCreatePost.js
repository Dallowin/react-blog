import { useState } from "react";
import "./ModalCreatePost.css"

export function ModalCreatePost({ addPost, changePost, closeModal, isEdit, editID, posts }) {
    // Поиск по ID в списке постов
    const editingValue = () => {
        return posts.find(post => post.uid === editID)
    }
    
    const [titleValue,  setTitleValue]    = useState(isEdit ? editingValue().title : "");
    const [descValue,   setDescValue]     = useState(isEdit ? editingValue().description : "");

    function validateForm (e) {
        e.preventDefault();

        if (!isEdit) {
            if (titleValue.length > 0 && descValue.length > 0) {
                // Добавление новой записи в список 
                addPost({
                    title: titleValue,
                    description: descValue
                }) 
    
                // Закрываем модальное окно после добавления записи
                closeModal('add')
            }
        } else {
            if (titleValue.length > 0 && descValue.length > 0) {
                // Отправление обновленных данных в список
                changePost({
                    uid: editingValue().uid,
                    createdAt: editingValue().createdAt,
                    title: titleValue,
                    description: descValue
                }) 

                // Закрываем модальное окно после изменения записи
                closeModal('edit')
            }
        }

        // Делаем перменные пустыми
        setTitleValue("")
        setDescValue("")
    }

    return(
        <div className="modal">
            <form className="create-form" onSubmit={validateForm}>
                
                <div className="modal-close" onClick={closeModal}></div>


                <label className="create-form__label"> Title
                    <input value={titleValue} onChange={e => setTitleValue(e.target.value)} className="create-form__input" type="text" name="title" placeholder="Enter Title"/>
                </label>

                <label className="create-form__label"> Description
                    <textarea value={descValue} onChange={e => setDescValue(e.target.value)} cols="25" rows="5" className="create-form__input" type="text" name="description" placeholder="Enter description"/>
                </label>

                <button className="button create-form__button">{ isEdit ? 'Edit' : 'Create' }</button>
            </form>
        </div>
    )
}